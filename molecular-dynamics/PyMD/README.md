## Introduction

I made this project to further study the basic theory of Molecular Dynamics and also to improve my python programing ability.

This project aims to build a module in pure python to directly perform MD simulation without any help from other software.

All codes develped here were inspired by the C++ MD project from Dr.Fan Zheyong, which you can find at https://github.com/brucefan1983/Molecular-Dynamics-Simulation

Anyone shall never publish or spread this project without my permission.

@ Author: Zheyi Zhang, China
@ Contact: zhangzy3033@163.com