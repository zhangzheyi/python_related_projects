## Introduction ##

This project was designed to simulate the condition of a room with IOT furnitures installed.

Run integration.py to perform the whole simulation process.

It also provides a child process for users to modify room conditions and furniture settings, at anytime during simulation running, by inputing several commands in command window, which is apparently used to simulate the root control from room owner.