import sys
import time
import multiprocessing as mp

from src import inputProcess
from src import simulationProcess

def main(start, length, speed_up):
    tic = time.time()
    
    inp = sys.stdin.fileno()
    queue  = mp.Queue()
    root = mp.Process(target=inputProcess, args=(queue,inp, ))
    root.start()
    time.sleep(0.3)
    
    simulationProcess(queue, start, length, speed_up)
    
    root.terminate()    
    
    toc = time.time()
    print('Total Time Spent: {:.2f} s'.format(toc - tic), flush=True)
    
if __name__ == '__main__':
    start  = 12  # xx o'clock
    length = 12  # hours 
    speed_up = 1200 # speed up for speed_up times compared to real time
    main(start, length, speed_up)
   
