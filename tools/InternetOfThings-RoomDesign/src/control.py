'''
This code stores the controler class 
It is used to connect all furnitures so as to achieve the IOT
It keeps all prefered settings of furnitures and would adjust them automatically if needed
Users can also change it at any time to overwrite preference setting
'''

class Controler:
    def __init__(self, platform) -> None:
        # could be installed on different platform, e.g., room, mobile phone...
        self._prefer_temperature = 25
        self._prefer_brightness  = 60 # max 100, sunny day with all lights on and curtain opened
        self._prefer_tvshow = 'friends'
        self._prefer_music  = 'welcome to Ottawa'
        self._prefer_bed_time = 23  # 24 hours
        self._prefer_getup_time = 7 # 24 hours
        self._current_time = 0
        self._platform = platform
        
    def checkState(self) -> None:
        print('Prefered room temperature is {}, prefered room brightness is {}'.format(self._prefer_temperature, self._prefer_brightness), flush=True)
        print('Prefered room temperature is {}, prefered room brightness is {}'.format(self._prefer_temperature, self._prefer_brightness), flush=True)
        
    def control(self, room) -> bool:
        self._tmp_kept = False # is temperature get adjusted done
        self._brt_kept = False # is brightness get adjusted done
        if room._someone_in_room:
            room.light._turn_on_degree = 0
            room.curtain._turn_on_degree = 100 # default
            
            self.keepTemperature(room)
            self.keepBrightness(room)
            if not room.mp._turn_on:
                room.mp.turnItOn(self._prefer_music)
            if not room.tv._turn_on:
                room.tv.turnItOn(self._prefer_tvshow)

        else:
            if self._current_time[1] > 18 or self._current_time[1] < 8:
                # curtain dont effect brightness at night
                room.curtain.turnItOff()
            else:
                room._brightness -= room.curtain.turnItOff()
            room._brightness -= room.light.turnItOff()
            room.mp.turnItOff()
            room.tv.turnItOff()
            room.ac.turnItOff()
    
    def keepTemperature(self, room):
        if abs(room._temperature - self._prefer_temperature) < 0.5:
            self._tmp_kept = True
        else:
            if room._temperature > self._prefer_temperature:
                room._temperature += room.ac.cooling()
            elif room._temperature < self._prefer_temperature:
                room._temperature += room.ac.heating()
        
    def keepBrightness(self, room) -> None:
        if int(self._current_time[1]) >= self._prefer_bed_time or int(self._current_time[1]) <= self._prefer_getup_time:
            # close curtain and turn off lights during bed time
            room._brightness -= room.light.turnItOff()
            room.curtain.turnItOff()
            
        elif int(self._current_time[1]) > self._prefer_getup_time and int(self._current_time[1]) <= 18:
            # day time trying to adjust curtain first
            # print(room._brightness, room.curtain._turn_on_degree, room.light._turn_on_degree)
            
            # keep it to be the same as the last adjust
            if room.curtain._turn_on_degree < 100:
                room._brightness -= room.curtain.turnItDown(100 - room.curtain._turn_on_degree)
            if room.light._turn_on_degree > 0:
                room._brightness += room.light.turnItOn(room.light._turn_on_degree)
                
            # print(room._brightness, room.curtain._turn_on_degree, room.light._turn_on_degree)
            if room._brightness < self._prefer_brightness:
                while abs(room._brightness - self._prefer_brightness) > 5:
                    if room.curtain._turn_on_degree < 100:
                        room._brightness += room.curtain.turnItUp(10) # every second increase 10 degrees  
                        continue
                    elif room.light._turn_on_degree < 100:
                        room._brightness += room.light.turnItUp(10) 
                        continue
                    else:
                        break
                # print(room._brightness, room.curtain._turn_on_degree, room.light._turn_on_degree)
            elif  room._brightness > self._prefer_brightness:
                while abs(room._brightness - self._prefer_brightness) > 5:
                    if room.curtain._turn_on_degree > 0:
                        room._brightness -= room.curtain.turnItDown(10) # every second increase 10 degrees  
                        continue
                    elif room.light._turn_on_degree > 0:
                        room._brightness -= room.light.turnItDown(10) 
                        continue 
                    else:
                        break 
            else:
                pass
            
        else:
            # night time trying to adjust light first
            
            # keep it to be the same as the last adjust
            if room.curtain._turn_on_degree < 100:
                room._brightness -= room.curtain.turnItDown(100 - room.curtain._turn_on_degree)
            if room.light._turn_on_degree > 0:
                room._brightness += room.light.turnItOn(room.light._turn_on_degree)
            
            if room._brightness < self._prefer_brightness:
                while abs(room._brightness - self._prefer_brightness) > 5:
                    if room.light._turn_on_degree < 100:
                        room._brightness += room.light.turnItUp(10) 
                        continue
                    elif room.curtain._turn_on_degree < 100:
                        room._brightness += room.curtain.turnItUp(10) # every second increase 10 degrees  
                        continue
                    else:
                        break 
                     
            elif  room._brightness > self._prefer_brightness:  
                while abs(room._brightness - self._prefer_brightness) > 5:
                    if room.light._turn_on_degree > 0:
                        room._brightness -= room.light.turnItDown(10) 
                        continue 
                    elif room.curtain._turn_on_degree > 0:
                        room._brightness -= room.curtain.turnItDown(10) # every second increase 10 degrees  
                        continue
                    else:
                        break 
            else:
                pass
        
        if abs(room._brightness - self._prefer_brightness) < 5:
            self._brt_kept = True
            
    def autoPlayPrefered(self, player) -> None:
        if type(player) == 'Television':
            player.play(self._prefer_tvshow)
        elif type(player) == 'MusicPlayer':
            player.play(self._prefer_music)

if __name__ == '__main__':
    controler = Controler()