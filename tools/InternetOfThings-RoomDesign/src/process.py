'''
This code stores two processes in program running,
i.e., condition simulation (time evolution) and input
'''
import os
import sys
import time
from .control   import Controler
from .furniture import Light
from .furniture import Curtain
from .furniture import Television
from .furniture import MusicPlayer
from .furniture import AirConditioner

from .condition import Outside
from .condition import Room

def rootHelp() -> str:
    help = \
'''
Root Options:
#################################################################################
exit                      End the whole process
x get in                  Simulate x people getting in room
x get out                 Simulate x people getting out of room
prefered T = x            Simulate setting prefered room temperature on controler
prefered B = x            Simulate setting prefered room brightness on controler
room state                Show room state (physical conditions, if people in)
controler state           Show controler preference set by user
#################################################################################
'''
    return help

def rootOperation(inputs, room, controler) -> None:
    if 'help' in inputs:
        print(rootHelp(), flush=True)
        time.sleep(0.5)
    elif 'get in' in inputs:
        room.getIn(int(inputs.split()[0]), controler._current_time[-1])  
    elif 'get out' in inputs:
        room.getOut(int(inputs.split()[0]), controler._current_time[-1])  
    elif 'prefered T = ' in inputs:
        controler._prefer_temperature = int(inputs.split('=')[1])
    elif 'prefered B = ' in inputs:
        controler._prefer_brightness = int(inputs.split('=')[1])   
    elif inputs == 'room state':
        room.checkState()
    elif inputs == 'controler state':
        controler.checkState()
    else:
        print('Invalid command', flush=True)

def inputProcess(queue, inp) -> None:
    # This process is used to simulate user interface
    # Users are allowed to input commands at any time in simulation (main) process
    # So as to realize the root control of things, e.g., default setting, someone get in room
    inputs = ''
    sys.stdin = os.fdopen(inp)
    print('A subprocess is used to simulate controler interface', flush=True)
    print('Input commands at any time to control things\n', flush=True)
    while True:
        # clear queue and put inputs into queue
        while not queue.empty():
            queue.get()
        queue.put(inputs)
        
        # get keyboard inputs
        inputs = sys.stdin.readline().strip('\n')
        
        if inputs == 'exit':
            print('Simulation ended', flush=True)
            continue
    
def simulationProcess(queue, start, length, speed_up) -> None:
    # adjust settings at any time through input process
    inputs = None
    world  = Outside(start)
    room   = Room('Living room')
    # speed_up = 600 # speed up process for speed_up times compared to real time
    room.installFurnitures(Light(room.name), Curtain(room.name), 
                            Television(room.name), MusicPlayer(room.name),
                            AirConditioner(room.name))
    
    controler = Controler('in_room')
    # controler2 = Controler('mobile_phone')
    
    for i in range(0, 3600 * length + 1):

        controler._current_time = world.getTime(i) # hour
        
        if not queue.empty():
            # pause process and try to get inputs
            inputs = queue.get()
            if inputs != '':
                # got inputs
                # then do certain things (root setting)
                if inputs == 'exit':
                    break
                
                rootOperation(inputs, room, controler)
        
        world.timeEvolution(i)
        world.effectRoomCondition(i, room)
        controler.control(room)
        
        if i % 3600 == 0:
            # print state every hour
            print('Outside Temperature at {} o\'clock is {:.2f} Celsius'.format(controler._current_time[1], world._temperature), flush=True)
            print('Outside Brightness at {} o\'clock is {:.2f}'.format(controler._current_time[1], world._brightness), flush=True)
            
            if room._someone_in_room:
                print('{} people in {} at {}'.format(room._people_count, room._name, controler._current_time[-1]), flush=True)

            print('Room temperature is {:.2f} Celsius, Room brightness is {:.2f}'.format(room._temperature, room._brightness), flush=True)
            print('Light turn-on degree {}, curtain turn-on degree {}\n'.format(room.light._turn_on_degree, room.curtain._turn_on_degree), flush=True)
            
        # speeding up in visual time
        world.delayOneSecond(1/speed_up)
        
if __name__ == '__main__':
    controler = Controler()