'''
This code stores all available furnitures functions for IOT
'''
class Furniture:
    def __init__(self) -> None:
        self._name = 'furniture'
        self._turn_on = False
    
    def __repr__(self) -> str:
        return self._name
    
    @property
    def name(self):
        return self._name
    
    def turnItOn(self) -> None:
        if self._turn_on == False:
            self._turn_on = True
            print('{} is turned on'.format(self), flush=True)
    
    def turnItOff(self) -> None:
        if self._turn_on == True:
            self._turn_on = False
            print('{} is turned off'.format(self), flush=True)
    
    def checkState(self) -> None:
        if self._turn_on:
            print('{} is on'.format(self), flush=True)
        else:
            print('{} is off'.format(self), flush=True)

class Light(Furniture):
    def __init__(self, name) -> None:
        super().__init__()
        self._name = name + ' light'
        self._brightness = 0 # generated brightness, 0 means turned off
        self._turn_on_degree = 0 # default, fully turned off
        
    def getBrightness(self) -> int:
        return  self._brightness
        
    def turnItOn(self, degree) -> int:
        self._turn_on_degree = 0
        super().turnItOn()
        return self.turnItUp(degree)
    
    def turnItOff(self) -> int:
        super().turnItOff()
        return self.turnItDown(100)
    
    def turnItUp(self, degree) -> int:
        if degree > 100 - self._turn_on_degree:
            degree = 100 - self._turn_on_degree
            
        self._turn_on_degree += degree
        
        if self._turn_on_degree > 0:
            self._turn_on = True
        # print('{} is turning up by {} degrees'.format(self._name, degree), flush=True)
        
        self._brightness = int(30 * self._turn_on_degree * 0.01)
        return int(30 * degree * 0.01)
    
    def turnItDown(self, degree) -> int:
        if degree > self._turn_on_degree:
            degree = self._turn_on_degree
        
        self._turn_on_degree -= degree

        if self._turn_on_degree == 0:
            self._turn_on = False
        # print('{} is turning down by {} degrees'.format(self._name, degree), flush=True) 
               
        self._brightness = int(30 * self._turn_on_degree * 0.01)
        return int(30 * degree * 0.01)
      
class Curtain(Furniture):
    def __init__(self, name) -> None:
        super().__init__()
        self._name = name + ' windows curtain'
        self._turn_on = True
        self._turn_on_degree = 100 # default, fully turned on
    
    def getBrightness(self) -> int:
        return  int(30 * self._turn_on_degree * 0.01)
    
    def turnItOn(self, degree) -> int:
        super().turnItOn()
        return self.turnItUp(degree)
    
    def turnItOff(self) -> int:
        super().turnItOff()
        return self.turnItDown(100)
    
    def turnItUp(self, degree) -> int:
        self._turn_on_degree += degree
        
        if self._turn_on_degree > 100:
            self._turn_on_degree = 100
            
        if self._turn_on_degree > 0:
            self._turn_on = True
        # print('{} is turning up by {} degrees'.format(self._name, degree), flush=True)
        return int(30 * degree * 0.01)
    
    def turnItDown(self, degree) -> int:
        self._turn_on_degree -= degree
        
        if self._turn_on_degree < 0:
            self._turn_on_degree = 0
            
        if self._turn_on_degree == 0:
            self._turn_on = False
        # print('{} is turning down by {} degrees'.format(self._name, degree), flush=True) 
               
        return int(30 * degree * 0.01)
    
    def checkState(self) -> None:
        super().checkState()
        if self._turn_on:
            print('Turn on degree is {}'.format(self._turn_on_degree), flush=True)
                  
class MusicPlayer(Furniture):
    def __init__(self, name) -> None:
        super().__init__()
        self._name = name + ' music player'
        self._playing = None
        
    def turnItOn(self, music) -> None:
        super().turnItOn()
        self.play(music)

    def turnItOff(self) -> None:
        super().turnItOff()
        self.stopPlay()   
    
    def play(self, music) -> None:
        self._playing = music
        print('{} starts playing music "{}"'.format(self, music), flush=True)
    
    def stopPlay(self) -> None:
        if self._playing != None:
            print('{} stops playing music'.format(self), flush=True) 
        self._playing = None
    
    def checkState(self) -> None:
        super().checkState()
        if self._turn_on:
            print('{} is playing music "{}"'.format(self, self._playing), flush=True)
    
class Television(Furniture):
    def __init__(self, name) -> None:
        super().__init__()
        self._name = name + ' television'
        self._playing = None
    
    def turnItOn(self, show) -> None:
        super().turnItOn()
        self.play(show)

    def turnItOff(self) -> None:
        super().turnItOff()
        self.stopPlay() 
    
    def play(self, show) -> None:
        self._playing = show
        print('{} starts playing show "{}"'.format(self, show), flush=True)   
    
    def stopPlay(self) -> None:
        if self._playing != None:
            print('{} stops playing show'.format(self), flush=True)  
        self._playing = None
    
    def checkState(self) -> None:
        super().checkState()
        if self._turn_on:
            print('{} is playing show "{}"'.format(self, self._playing), flush=True)
            
class AirConditioner(Furniture):
    def __init__(self, name) -> None:
        super().__init__()
        self._name = name + ' air conditioner'
        # self._prefer_temperature = temp
        
    def cooling(self) -> float:
        return -0.02 
        
    def heating(self) -> float:
        return 0.05

if __name__ == '__main__':
    furn = Furniture()
    light1 = Light('living room')
    furn.turnItOn()
    furn.checkState()
    light1.checkState()
    print(light1.turnItOn())