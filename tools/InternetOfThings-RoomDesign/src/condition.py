'''
This code stores physical conditions, i.e., outside world, room
'''
import time
import numpy as np

class Outside:
    def __init__(self, start_hour):
        self._start_hour = start_hour
        self._brightness = 70  # sunny
        self._temperature= 15  # degree celsius
        print('This day starts at {} o\'clock'.format(self._start_hour), flush=True)
    
    def getTime(self, current_time) -> str:
        day    = 1
        second = round(current_time % 60)
        minute = round(current_time // 60)
        hour   = round(minute // 60)
        minute = minute % 60
        
        hour   += self._start_hour
        day    += hour // 24
        hour   = hour % 24
        return day, hour, minute, second, 'Day {}, {}:{:02d}:{:02d}'.format(day, hour, minute, second) 
    
    def delayOneSecond(self, t):
    # t is real time in s, i.e. 0.001 for 1ms
        start,end = 0,0
        start = time.perf_counter()  
        while(end - start < t):
            end = time.perf_counter()
    
    def timeEvolution(self, time_passed):
        time = self.getTime(time_passed)
        
        self.temperatureEvo(time[1])
        self.brightnessEvo(time[1])
        
        if all(t == 0 for t in time[1:4]):
            # print state every day
            print('Day {} starts'.format(time[0]), flush=True)
            
        
    def temperatureEvo(self, hour):
        # with this formula, temperature floats in the range of (-9, 9) in one day, corresponding to winter
        self._temperature = 50 * np.exp(-0.5 * ((hour - 12)/4 - 1/4)**2) / np.sqrt(2 * np.pi) # normal distribution
        self._temperature += -10  # winter
        
    def brightnessEvo(self, hour):
        # brightness < 20 stands for night (completely sunset)
        # 20 < brightness < 40 stands for early morning and dusk
        # brightness > 40 stands for normal day time
        # with this formula, brightness floats in the range of (5, 70) in one day, corresponding to fine day
        self._brightness = (100 * np.exp(-0.5 * ((hour - 12)/4 - 1/4 )**2) / np.sqrt(2 * np.pi)) * 1.6 # normal distribution
        self._brightness += 5  # fine day
        
    def effectRoomCondition(self, time_passed, *args):
        hour = self.getTime(time_passed)[1]
    
        for room in args:
            room._temperature += (self._temperature - room._temperature) * 0.001
            room._brightness  += self._brightness - room._brightness
            if time_passed == 0:
                room._temperature = self._temperature
            # if (time_passed % 3600) == 0:
            #    print('{} Temperature at {} o\'clock is {:.2f}'.format(room._name, hour, room._temperature), flush=True)
            #    print('{} brightness at {} o\'clock is {:.2f}'.format(room._name, hour, room._brightness), flush=True)
        
class House:
    def __init__(self):
        self._rooms = []
        
    def addRoom(self, room):
        self._rooms.append(room)
        
class Room:
    def __init__(self, room) -> None:
        self._name = room
        self._people_count = 0
        self._someone_in_room  = False
        self._temperature = 15
        self._brightness  = 100 # sunny day with lights on
        
    def __repr__(self) -> str:
        return self._name
    
    @property
    def name(self):
        return self._name
    
    def checkState(self) -> None:
        print('{} people in {}'.format(self._people_count, self._name), flush=True)
        print('Room temperature is {:.2f}, room brightness is {:.2f}'.format(self._temperature, self._brightness), flush=True)
    
    def getIn(self, number, intime) -> None:
        self._someone_in_room  = True
        self._people_count += number
        print('{} people getting into {} at {}'.format(number, self._name, intime), flush=True)
    
    def getOut(self, number, offtime) -> None:
        if number <= self._people_count:
            self._people_count -= number
            print('{} people getting off {} at {}'.format(number, self._name, offtime), flush=True)
        else:
            print('Failed! Only {} people in {}'.format(self._people_count, self._name), flush=True)
            
        if self._people_count == 0:
            self._someone_in_room  = False
    
    def installFurnitures(self, *args):
        for item in args:
            name = item.name
            if 'light' in name:
                self.light = item
            elif 'curtain' in name:
                self.curtain = item
            elif 'television' in name:
                self.tv = item
            elif 'music player' in name:
                self.mp = item
            elif 'air conditioner' in name:
                self.ac = item
            else:
                print('Unsupported furniture "{}" in IOT system'.format(name), flush=True)

if __name__ == '__main__':
    tic = time.time()
    inputs = None
    start  = 12  # xx o'clock
    totime = 12  # hours 
    world  = Outside(start)
    room1  = Room('Living room', start)
    speed_up = 1200
    for i in range(0, 3600 * totime + 1):
        if inputs != None:
            # pause time
            world.delayOneSecond(10)
            # then do certain things
            
            
        world.timeEvolution(i)
        world.effectRoomCondition(i, room1)
        
        # if i % 7200 == 0:
        #     print(world.getTime(i)[-1], flush=True)
        # speed up for speed_up times compared to real time
        world.delayOneSecond(1/speed_up) 
        
    toc = time.time()
    print('Total Time Spent: {:.2f}'.format(toc - tic), flush=True)
    