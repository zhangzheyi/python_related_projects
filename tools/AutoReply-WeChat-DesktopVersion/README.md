## Introduciton

This a self-made program to automatically reply WeChat message on computer (not for mobile phone)

How to use it:

1. Download all the codes and folders to your machine

2. Set parameters in the bottom of autoReply.py

3. Submit running in powershell or cmd (make sure you got python3 on your machine)

4. Wait and appreciate its performance
