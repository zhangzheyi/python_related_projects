#-*- coding:utf-8 -*-
# Author: yiyi
# Email : zhangzy3033@163.com
import pyautogui as pyg
import keyboard
import requests
import urllib
import unicodedata
import os
import time
import sys
sys.path.append(os.path.dirname(os.path.abspath(__file__)))
import const

def countDown(seconds):
    # count down seconds
    for i in range(seconds,0,-1):
        print(const.prompts['time'].format(str(i)),end='', flush=True)
        time.sleep(1)
    print()

def alertInfo(msg, name, output='OK'):
    b = pyg.alert(text=msg, title=name, button=output)
    #b = pyg.confirm(text=msg, title=name, button=output)
    print(b)

def reqInfo(msg, name):
    b = pyg.prompt(text=msg, title=name, button=output)
    return b
    
def getMousePos():
    cmx,cmy = pyg.position()
    return cmx,cmy

def callBack(x):
    a = kb.KeyboardEvent('down', 28, exit_button)
    if x.event_type == 'down' and x.name == a.name:
        print(const.prompts['end'], flush=True)
        isExit = True

def isNewMsg(news, default):
    if news == default:
        return False
    else:
        return True
    
def isChinese(char):
    if 'CJK' in unicodedata.name(char):
        return True
    else:
        return False

def qingyunke(msg):
    # get response from qingyunke's website
    url = 'http://api.qingyunke.com/api.php?key=free&appid=0&msg={}'.format(urllib.parse.quote(msg))
    #url = 'http://api.qingyunke.com/api.php?key=free&appid=0&msg={}'.format(msg)
    html = requests.get(url)
    return html.json()["content"]

def getResponse(name, msg):
    # deliver received message to web then get response
    print("{}:".format(name), msg, flush=True)
    res = const.prompts['please']
    if isChinese(msg.split()[0][0]) and isChinese(msg.split()[-1][0]):
        time.sleep(0.5)
        res = qingyunke(msg) 
    print(const.prompts['res'], res, flush=True)
    return res

if __name__ == '__main__':
    print(pyg.onScreen(2559,1439))
    cmx, cmy = pyg.position()
    msg = '师傅你是做什么工作的'
    getResponse('对方', msg)
