#-*- coding:utf-8 -*-
# Author: yiyi
# Email : zhangzy3033@163.com
import pyautogui as pyg
import pyperclip as pyc
import pygetwindow as gw
import os
import time
import sys
sys.path.append(os.path.dirname(os.path.abspath(__file__)))
import const

def openWindow(app):
    # open certain window
    windows = gw.getAllWindows()
    item = None
    
    for window in windows:
        if app in window.title:
            item = window
            break
        
    try:
        item.restore()
        item.activate()
    except AttributeError:
        print(const.prompts['appnotfound'].format(app), flush=True)
        exit()
    return item
    
def initWindow(app, window_size=[800, 600]):
    # open and modify its position
    item = openWindow(app)
    pyg.moveTo(item.left+10, item.top+10, duration=0.1)
    pyg.dragTo(100, 200, duration=0.2, button='left')
    item.resizeTo(window_size[0], window_size[1])
    pyg.moveRel(100, 100, duration=0.1)
    pyg.scroll(1000)
    pyg.scroll(1000)
    #item.minimize()
    return item

def miniWindow(app):
    # minimize app window
    item = openWindow(app)
    item.minimize()
    
def findChat(person_name, profile, profile_msg, inter=0.1):
    # find this person and click its profile
    # make sure chat box appearing on right side
    isFound  = False
    location = None
    startime = time.time()
    
    while not isFound and any(i != None for i in [profile, profile_msg]):
        if time.time()-startime > 3:
            break
        try:
            location = pyg.locateCenterOnScreen(profile, confidence=0.9)
        except Exception:
            try:
                location = pyg.locateCenterOnScreen(profile_msg, confidence=0.9)
            except Exception:
                pyg.scroll(-500)
                time.sleep(0.5)
            else:
                pyg.moveTo(location.x, location.y, duration=0.2)
                isFound = True
        else:
            isFound = True
            
    if isFound:
        print(const.prompts['enter'], flush=True)
        pyg.click(location.x, location.y, 1, duration=inter)
    else:
        print(const.prompts['search'], flush=True)
        searchbar = pyg.locateCenterOnScreen(const.search, confidence=0.9)
        pyc.copy(person_name)
        pyg.click(searchbar.x+50, searchbar.y, 1, duration=inter)    
        pyg.hotkey('ctrl','v')
        time.sleep(0.5)
        pyg.press('enter')
        print(const.prompts['enter'], flush=True)
        
    print(const.prompts['delete'], flush=True)
    pyg.press('backspace',10,0.05)
    pyg.press('delete',10,0.05)
    return location

def copyChatMsg(default, times=3, inter=0.1):
    # find and copy new message through locating relative position
    # able to merge single-line message for three times
    pyc.copy(default)
    msgs = default
    split = ', '
    location = pyg.locateCenterOnScreen(const.scshot, confidence=0.9)
    pyg.moveTo(location.x, location.y-200, duration=inter)
    pyg.scroll(-1000)
    pyg.moveTo(location.x, location.y, duration=inter)
    
    for i in range(times):
        pyg.moveRel(0 , -60, duration=inter)
        pyg.click(button='right')
        pyg.moveRel(10 , 0, duration=0.2)
        pyg.click(button='left')
        pyg.moveRel(-10 , 0, duration=inter)
        tmp = pyc.paste()
        if tmp == default or tmp + split == msgs: # no new message
            break
        elif tmp != msgs and tmp in msgs: # repeated message
            continue
        else: # merge new message with the last one
            msgs = tmp + split + msgs
            
    pyg.click(location.x, location.y+50, 1, duration=inter)
    return msgs.rsplit(',',1)[0]

if __name__ == '__main__':
    app = '微信'
#     wind = openWindow(app)
    wind = initWindow(app)
