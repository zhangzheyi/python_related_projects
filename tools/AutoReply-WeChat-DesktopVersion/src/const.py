#-*- coding:utf-8 -*-
# Author: yiyi
# Email : zhangzy3033@163.com
import os
import imghdr
import pyautogui as pyg

app     = '微信'
scshot  = './resource/wechat/shot.png'
search  = './resource/wechat/search.png'
default = ''
'''
# English
prompts = {'show':'Welcome to use this open-source program written by yiyi! It is designed to auto-response messages on WeChat',
           'init':'Initializing program ...','res':'Response:',
           'lackModule':'Error! Make sure all the packages below are installed:',
           'end':'Program ended','time':'{}s ','please':'Please speaking Chinese to me!',
           'appnotfound':'{} Not found\nPlease make sure this app is opened or is minimized in taskbar',
           'enter':'Entering chat dialog','search':'Can not find chat by personal profile\nTrying to search name',
           'delete':'Deleting unsended message','name':'Person name is: {}',
           'soon':'Program will be running soon, you can close it in break-time interval between each round',
           'music':'Let us drop some beats: {}','count':'Counting down: ',
           'newround':'\nStart a new round','msgnotfound':'Not found new message',
           'default':'Display default message:','stop':"You got {}s to stop program by pressing 'ctrl'+'c'"}

'''
# Chinese
prompts = {'show':'这是一张纸编写的电脑微信自动回复开源小脚本，欢迎使用！',
           'init':'正在初始化 ...','res':'回复:',
           'lackModule':'出错了! 请确保你的python安装了这些库:',
           'end':'程序结束','time':'{}s ','please':'看不懂, 求你说中文!',
           'appnotfound':'找不到 {}\n请确认这个软件已经打开或者处于任务栏中',
           'enter':'进入聊天对话框','search':'找不到这个头像\n搜一下名字试试',
           'delete':'删除残留的草稿信息','name':'这人名字是: {}',
           'soon':'程序马上启动, 你可以在每轮运行的间隔中使用快捷键关闭程序',
           'music':'放点BGM: {}','count':'倒计时: ',
           'newround':'\n开始新一轮运行','msgnotfound':'没发现新消息',
           'default':'这是默认的空白消息:','stop':"你有 {}s 时间来使用ctrl+c停止程序"}


curdir  = os.path.dirname(os.path.abspath(__file__))
resdir  = os.path.join(os.path.abspath(os.path.join(curdir, '..')), 'resource')
weChat  = os.path.join(resdir, 'wechat')
theBgm  = os.path.join(os.path.join(resdir, 'music'),'LoseControl.mp3')
dirs    = {}
imgType = ['jpg','png','jpeg','bmp']

screen_size    = [pyg.size().width, pyg.size().height]
package_needed = ['pyautogui','pyperclip','keyboard',
                  'requests','pygetwindow', 'pygame']

def initialize():
    print(prompts['show'], flush=True)
    print(prompts['init'], flush=True)
    
    try:
        import pyautogui as pyg
        import pyperclip
        import keyboard
        import requests
        import pygetwindow
        import pygame
    except ModuleNotFoundError:
        print(prompts['lackModule'], flush=True)
        for package in package_needed:
            print(package,end=', ')
        print(flush=True)
        print(prompts['end'], flush=True)
        
    for file in os.listdir(weChat):
        tmpath = os.path.join(weChat, file)
        if os.path.isfile(tmpath) and imghdr.what(tmpath) in imgType:
            index = file.split('.')[0]
            dirs[index] = tmpath
