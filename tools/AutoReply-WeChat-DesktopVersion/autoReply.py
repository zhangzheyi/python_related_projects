#-*- coding:utf-8 -*-
# Author: yiyi
# Email : zhangzy3033@163.com
import pyautogui as pyg
import pyperclip as pyc
import time
import os
from src import const
from src import getInfo
from src import windows

def preSet(seconds, playBgm=True):
    const.initialize()
    # print some infos befor get started
    print(const.prompts['soon'], flush=True)
    
    if playBgm:
        from pygame import mixer
        mixer.init()
        mixer.music.load(const.theBgm)
        mixer.music.set_volume(0.2)
        print(const.prompts['music'].format(os.path.basename(const.theBgm)), flush=True)
        mixer.music.play(-1) # loop play
    
    try:
        print('Counting down: ',end='', flush=True)
        getInfo.countDown(seconds)
    except KeyboardInterrupt:
        print('\n'+const.prompts['end'], flush=True)
        exit()

def mainFlow(person_name, default, seconds, profile=None, profile_msg=None):
    # execute the entire process every given seconds
    if len(person_name.split()) > 1:
        # only allowed to use person name to find multiple dialogs
        profile = None
        profile_msg = None
    while True:
        print(const.prompts['newround'], flush=True)
        windows.initWindow(const.app)
        for person in person_name.split():
            print(const.prompts['name'].format(person), flush=True)
            profile_location = windows.findChat(person, profile, profile_msg)
            msg = windows.copyChatMsg(default)
            if getInfo.isNewMsg(msg,default): 
                res = getInfo.getResponse(person,msg)
                pyc.copy(res)
                pyg.hotkey('ctrl','v')
                pyg.hotkey('ctrl','enter')
            else:
                print(const.prompts['msgnotfound'], flush=True)
                print(const.prompts['default'], msg, flush=True)
        afterSendState()
        print(const.prompts['stop'].format(seconds), flush=True)
        try:
            getInfo.countDown(seconds)
        except KeyboardInterrupt:
            print('\n'+const.prompts['end'],flush=True)
            exit()

def afterSendState(inter=0.1):
    # go back to enter a certain dialog
    # wait for the next round of operation
    windows.initWindow(const.app)
    pyg.click(button='left')
    windows.miniWindow(const.app)
#     pyg.moveRel(20, 20, duration=inter)
#     pyg.click(button='left')

if __name__ == '__main__':
    
    person_name = '测试2' # seperate by space e.g. 'person1 person2'
    profile_msg = None # './resource/test_msg.png' # None
    profile = './resource/test2.png' # None
    # start running
    preSet(3, playBgm=True)
    mainFlow(person_name, const.default, 10, profile, profile_msg)

